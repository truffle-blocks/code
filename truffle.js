// t r u f f l e Copyright (C) 2012 FoAM vzw   \_\ __     /\
//                                          /\    /_/    / /  
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

truffle.main={}

truffle.main.world=null;
truffle.main.time=0;

var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||  
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

truffle.main.init=function(blockly) {

    truffle.main.world=new truffle.world()
    requestAnimationFrame(truffle.main.loop);

    ////////////

    window.Blockly = blockly;

    window.onbeforeunload = function() {
        if (Blockly.mainWorkspace.getAllBlocks().length > 1) {
            return 'Leaving this page will result in the loss of your work.';
        }
        return null;
    };

    // Load the editor with a starting block.
    var code='<xml><block type="controls_for" inline="true" x="6" y="-268"><variable name="x" i="0"></variable><value i="0"><block type="math_number"><title i="0">0</title></block></value><value i="1"><block type="math_number"><title i="0">10</title></block></value><statement i="0"><block type="controls_for" inline="true"><variable name="y" i="0"></variable><value i="0"><block type="math_number"><title i="0">0</title></block></value><value i="1"><block type="math_number"><title i="0">10</title></block></value><statement i="0"><block type="variables_set" inline="false"><title i="1">entity</title><value i="0"><block type="create_sprite_entity" inline="false"><value i="0"><block type="create_vec3" inline="false"><value i="0"><block type="variables_get"><title i="1">x</title></block></value><value i="1"><block type="variables_get"><title i="1">y</title></block></value><value i="2"><block type="math_random_int" inline="true"><value i="0"><block type="math_number"><title i="0">0</title></block></value><value i="1"><block type="math_number"><title i="0">1</title></block></value></block></value></block></value><value i="1"><block type="choose" inline="false"><value i="0"><block type="lists_create_with" inline="false"><mutation items="3"></mutation><value i="0"><block type="text"><title i="1">images/grey-cube.png</title></block></value><value i="1"><block type="text"><title i="1">images/yellow-cube.png</title></block></value><value i="2"><block type="text"><title i="1">images/purple-cube.png</title></block></value></block></value></block></value></block></value></block></statement></block></statement><next><block type="variables_set" inline="false"><title i="1">robot</title><value i="0"><block type="create_sprite_entity" inline="false"><value i="0"><block type="create_vec3" inline="false"><value i="0"><block type="math_number"><title i="0">7</title></block></value><value i="1"><block type="math_number"><title i="0">5</title></block></value><value i="2"><block type="math_number"><title i="0">1</title></block></value></block></value><value i="1"><block type="text"><title i="1">images/bbot-south.png</title></block></value></block></value><next><block type="variables_set" inline="false"><title i="1">robot-sprite</title><value i="0"><block type="on_entity_get"><title i="1">robot</title><title i="2">get_root</title></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">needs_update</title><value i="0"><block type="logic_boolean"><title i="0">true</title></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">speed</title><value i="0"><block type="math_number"><title i="0">0.01</title></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">set_logical_pos</title><value i="0"><block type="create_vec3" inline="false"><value i="0"><block type="math_number"><title i="0">8</title></block></value><value i="1"><block type="math_number"><title i="0">5</title></block></value><value i="2"><block type="math_number"><title i="0">1</title></block></value></block></value><next><block type="variables_set" inline="false"><title i="1">fidget</title><value i="0"><block type="func"><statement i="0"><block type="on_sprite_set" inline="false"><title i="1">robot-sprite</title><title i="2">change_bitmap</title><value i="0"><block type="choose" inline="false"><value i="0"><block type="lists_create_with" inline="false"><mutation items="4"></mutation><value i="0"><block type="text"><title i="1">images/bbot-north.png</title></block></value><value i="1"><block type="text"><title i="1">images/bbot-south.png</title></block></value><value i="2"><block type="text"><title i="1">images/bbot-east.png</title></block></value><value i="3"><block type="text"><title i="1">images/bbot-west.png</title></block></value></block></value></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">set_logical_pos</title><value i="0"><block type="vadd" inline="false"><value i="0"><block type="on_entity_get"><title i="1">robot</title><title i="2">logical_pos</title></block></value><value i="1"><block type="create_vec3" inline="false"><value i="0"><block type="math_random_int" inline="true"><value i="0"><block type="math_number"><title i="0">-1</title></block></value><value i="1"><block type="math_number"><title i="0">1</title></block></value></block></value><value i="1"><block type="math_random_int" inline="true"><value i="0"><block type="math_number"><title i="0">-1</title></block></value><value i="1"><block type="math_number"><title i="0">1</title></block></value></block></value><value i="2"><block type="math_number"><title i="0">0</title></block></value></block></value></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">on_reached_dest</title><value i="0"><block type="variables_get"><title i="1">fidget</title></block></value></block></next></block></next></block></statement></block></value><next><block type="on_entity_set" inline="false"><title i="1">robot</title><title i="2">on_reached_dest</title><value i="0"><block type="variables_get"><title i="1">fidget</title></block></value></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></xml>';
    var xml = Blockly.Xml.textToDom(code);
    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);

    truffle.main.execute();
}


truffle.main.loop=function(timestamp) {
/*    var canvas=document.getElementById('canvas')
    var ctx=canvas.getContext('2d');
    ctx.fillStyle = "#00ff00";
    ctx.fillRect(0,0,canvas.width,canvas.height);
*/
    truffle.main.world.update(truffle.main.time);
    truffle.main.time++;
    requestAnimationFrame(truffle.main.loop);
}

truffle.main.run = function() {
    document.getElementById('runButton').style.display = 'none';
    document.getElementById('resetButton').style.display = 'inline';
    Blockly.mainWorkspace.traceOn(true);
    truffle.main.execute();
};

truffle.main.reset = function() {
    document.getElementById('runButton').style.display = 'inline';
    document.getElementById('resetButton').style.display = 'none';
    Blockly.mainWorkspace.traceOn(false);
    truffle.main.world.clear();
};

truffle.main.execute = function() {    
    var code = Blockly.Generator.workspaceToCode('JavaScript');
    try {
        eval(code);
    } catch (e) {
        // A boolean is thrown for normal termination.
        // Abnormal termination is a user error.
        if (typeof e != 'boolean') {
            alert(e);
        }
    }
};

truffle.main.save = function() {
    var code=Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var div=document.createElement("div");   
    div.id = "foo";
    div.innerText = code.innerHTML;
    document.getElementById("code").appendChild(div);
}

/**
 * Show the user's code in raw JavaScript.
 */
truffle.main.show_code = function() {
    var code = Blockly.Generator.workspaceToCode('JavaScript');
    var div=document.createElement("div");   
    div.id = "foo";
    div.innerHTML = "<pre><code>"+code+"</code></pre>";
    document.getElementById("code").appendChild(div);
};

// API

truffle.main.move = function(direction, id) {
};

truffle.main.turn = function(direction, id) {
};

truffle.main.isWall = function(direction) {
};
