/**
 * Blockly Demo: Maze
 *
 * Copyright 2012 Google Inc.
 * http://code.google.com/p/google-blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Demonstration of Blockly: Solving a maze.
 * @author fraser@google.com (Neil Fraser)
 */

// Extensions to Blockly's language and JavaScript generator.

// Define Language and JavaScript, in case this file is loaded too early.
if (!Blockly.Language) {
  Blockly.Language = {};
}
Blockly.JavaScript = Blockly.Generator.get('JavaScript');

Blockly.Language.func = {
    category: "Truffle",
    helpUrl: '',
    init: function() {
        this.setColour(290);
        this.addTitle("function");
        this.addInput('do', Blockly.NEXT_STATEMENT);
        this.setTooltip('A function');
        this.setOutput(true);
    }
};

Blockly.JavaScript.func = function() {
    var branch = Blockly.JavaScript.statementToCode(this, 0);
    var code = 'function () {\n' + branch + '}\n';
    return code;
};

///////////////////////////////////////////////////

Blockly.Language.on_entity_set = {
    category: "Truffle",
    helpUrl: '',
    vars: ["speed", "update_freq", "needs_update", "on_reached_dest"],
    methods: ["set_logical_pos", "set_tile_pos", "hide"],
    init: function() {
        this.setColour(290);
        this.addTitle('on entity set');
        this.addTitle(new Blockly.FieldDropdown(
            Blockly.Variables.dropdownCreate, Blockly.Variables.dropdownChange))
            .setText('item');
        this.addTitle(new Blockly.FieldDropdown(function() {
            return Blockly.Language.on_entity_set.vars.concat(
                Blockly.Language.on_entity_set.methods);
        }));
        this.addInput('', Blockly.INPUT_VALUE);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('');
    },
    getVars: function() {
        return [this.getTitleText(1)];
    },
    renameVar: function(oldName, newName) {
        if (Blockly.Names.equals(oldName, this.getTitleText(1))) {
            this.setTitleText(newName, 1);
        }
    }
};

Blockly.JavaScript.on_entity_set = function() {
    // Variable setter.
    var argument0 = Blockly.JavaScript.valueToCode(this, 0, true) || '0';
    var varName = Blockly.JavaScript.variableDB_.getName(this.getTitleText(1),
                                                         Blockly.Variables.NAME_TYPE);
    var attr=this.getTitleText(2);
    if (truffle.contains(Blockly.Language.on_entity_set.methods,attr)) {
        if (attr=="set_logical_pos")
        {
            return varName+'.'+attr+'(truffle.main.world,'+argument0+');\n';
        }
        else
        {
            return varName+'.'+attr+'('+argument0+');\n';
        }
    }
    return varName+'.'+attr+'='+argument0+';\n';
};

///////////////////////////////////////////////////

Blockly.Language.on_entity_get = {
    category: "Truffle",
    helpUrl: '',
    vars: ["logical_pos", "speed", "tile_pos", "move_time", "update_freq", "needs_update"],
    methods: ["get_root"],
    init: function() {
        this.setColour(290);
        this.addTitle('on entity get');
        this.addTitle(new Blockly.FieldDropdown(
            Blockly.Variables.dropdownCreate, Blockly.Variables.dropdownChange))
            .setText('item');
        this.addTitle(new Blockly.FieldDropdown(function() {
            return Blockly.Language.on_entity_get.vars.concat(
                Blockly.Language.on_entity_get.methods);
        }));
        this.setOutput(true);
        this.setTooltip('');
    },
    getVars: function() {
        return [this.getTitleText(1)];
    },
    renameVar: function(oldName, newName) {
        if (Blockly.Names.equals(oldName, this.getTitleText(1))) {
            this.setTitleText(newName, 1);
        }
    }
};

Blockly.JavaScript.on_entity_get = function() {
    // Variable setter.
    var varName = Blockly.JavaScript.variableDB_.getName(this.getTitleText(1),
                                                         Blockly.Variables.NAME_TYPE);
    var attr=this.getTitleText(2);
    if (truffle.contains(Blockly.Language.on_entity_get.methods,attr)) {
        return varName+'.'+attr+'()';
    }
    return varName+'.'+attr;
};

///////////////////////////////////////////////////

Blockly.Language.on_sprite_set = {
    category: "Truffle",
    helpUrl: '',
    vars: [],
    methods: ["set_pos", "set_scale", "set_rotate", "set_colour", "set_offset_colour", "mouse_down", "mouse_up", "mouse_over", "mouse_out", "change_bitmap"],
    init: function() {
        this.setColour(290);
        this.addTitle('on sprite set');
        this.addTitle(new Blockly.FieldDropdown(
            Blockly.Variables.dropdownCreate, Blockly.Variables.dropdownChange))
            .setText('item');
        this.addTitle(new Blockly.FieldDropdown(function() {
            return Blockly.Language.on_sprite_set.vars.concat(
                Blockly.Language.on_sprite_set.methods);
        }));
        this.addInput('', Blockly.INPUT_VALUE);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('');
    },
    getVars: function() {
        return [this.getTitleText(1)];
    },
    renameVar: function(oldName, newName) {
        if (Blockly.Names.equals(oldName, this.getTitleText(1))) {
            this.setTitleText(newName, 1);
        }
    }
};

Blockly.JavaScript.on_sprite_set = function() {
    // Variable setter.
    var argument0 = Blockly.JavaScript.valueToCode(this, 0, true) || '0';
    var varName = Blockly.JavaScript.variableDB_.getName(this.getTitleText(1),
                                                         Blockly.Variables.NAME_TYPE);
    var attr=this.getTitleText(2);
    if (truffle.contains(Blockly.Language.on_sprite_set.methods,attr)) {
        return varName+'.'+attr+'('+argument0+');\n';
    }
    return varName+'.'+attr+'='+argument0+';\n';
};

///////////////////////////////////////////////////

Blockly.Language.on_sprite_get = {
    category: "Truffle",
    helpUrl: '',
    vars: ["pos","width","height",],
    methods: [],
    init: function() {
        this.setColour(290);
        this.addTitle('on sprite get');
        this.addTitle(new Blockly.FieldDropdown(
            Blockly.Variables.dropdownCreate, Blockly.Variables.dropdownChange))
            .setText('item');
        this.addTitle(new Blockly.FieldDropdown(function() {
            return Blockly.Language.on_sprite_get.vars.concat(
                Blockly.Language.on_sprite_get.methods);
        }));
        this.setOutput(true);
        this.setTooltip('');
    },
    getVars: function() {
        return [this.getTitleText(1)];
    },
    renameVar: function(oldName, newName) {
        if (Blockly.Names.equals(oldName, this.getTitleText(1))) {
            this.setTitleText(newName, 1);
        }
    }
};

Blockly.JavaScript.on_sprite_get = function() {
    // Variable setter.
    var varName = Blockly.JavaScript.variableDB_.getName(this.getTitleText(1),
                                                         Blockly.Variables.NAME_TYPE);
    var attr=this.getTitleText(2);
    if (truffle.contains(Blockly.Language.on_sprite_get.methods,attr)) {
        return varName+'.'+attr+'()';
    }
    return varName+'.'+attr;
};

////////////////////////////////////////////////

Blockly.Language.create_vec2 = {
    // Block for moving forward or backwards.
    category: 'Vector',
    helpUrl: '',
    init: function() {
        this.setColour(259);
        this.addTitle('vec2');
        this.addInput('x', Blockly.INPUT_VALUE);
        this.addInput('y', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Create a new sprite');
        this.itemCount_=2;
    }
};

Blockly.JavaScript.create_vec2 = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return 'new truffle.vec2('+code.join(',')+')\n';
};

Blockly.Language.create_vec3 = {
    // Block for moving forward or backwards.
    category: 'Vector',
    helpUrl: '',
    init: function() {
        this.setColour(259);
        this.addTitle('vec3');
        this.addInput('x', Blockly.INPUT_VALUE);
        this.addInput('y', Blockly.INPUT_VALUE);
        this.addInput('z', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Create a new sprite');
        this.itemCount_=3;
    }
};

Blockly.JavaScript.create_vec3 = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return 'new truffle.vec3('+code.join(',')+')\n';
};

//////////////////////////////////////////////////////

Blockly.Language.vadd = {
    // Block for moving forward or backwards.
    category: 'Vector',
    helpUrl: '',
    init: function() {
        this.setColour(259);
        this.addTitle('vadd');
        this.addInput('a', Blockly.INPUT_VALUE);
        this.addInput('b', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Add the vectors together');
        this.itemCount_=2;
    }
};

Blockly.JavaScript.vadd = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return code[0]+'.add('+code[1]+')';
};




Blockly.Language.choose = {
    // Block for moving forward or backwards.
    category: 'Lists',
    helpUrl: '',
    init: function() {
        this.setColour(290);
        this.addTitle('choose');
        this.addInput('in list', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Choose a random element of the list');
        this.itemCount_=1;
    }
};

Blockly.JavaScript.choose = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return 'truffle.choose(' + code[0]  + ')';
};

///////////////////////////////////////////////////////

Blockly.Language.create_sprite = {
    // Block for moving forward or backwards.
    category: 'Truffle',
    helpUrl: '',
    init: function() {
        this.setColour(290);
        this.addTitle('sprite');
        this.addInput('position', Blockly.INPUT_VALUE);
        this.addInput('texture', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Create a new sprite');
        this.itemCount_=2;
    }
};

Blockly.JavaScript.create_sprite = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return 'truffle.main.world.add_sprite(new truffle.sprite(' + code.join(',')  + '))\n';
};

Blockly.Language.create_sprite_entity = {
    // Block for moving forward or backwards.
    category: 'Truffle',
    helpUrl: '',
    init: function() {
        this.setColour(290);
        this.addTitle('sprite entity');
        this.addInput('position', Blockly.INPUT_VALUE);
        this.addInput('texture', Blockly.INPUT_VALUE);
        this.setOutput(true);
        this.setTooltip('Create a new sprite');
        this.itemCount_=2;
    }
};

Blockly.JavaScript.create_sprite_entity = function() {
    var code = new Array(this.itemCount_);
    for (var n = 0; n < this.itemCount_; n++) {
        code[n] = Blockly.JavaScript.valueToCode(this, n, true) || 'null';
    }
    return 'new truffle.sprite_entity(truffle.main.world,' + code.join(',')  + ')\n';
};





/*

Blockly.Language.maze_move = {
  // Block for moving forward or backwards.
  category: 'Maze',
  helpUrl: 'http://code.google.com/p/google-blockly/wiki/Move',
  init: function() {
    this.setColour(290);
    this.addTitle('move');
    var dropdown = new Blockly.FieldDropdown(function() {
      return Blockly.Language.maze_move.DIRECTIONS;
    });
    this.addTitle(dropdown);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Moves the mouse forward or backward one space.');
  }
};

Blockly.Language.maze_move.DIRECTIONS = ['forward', 'backward'];

Blockly.JavaScript.maze_move = function() {
  // Generate JavaScript for moving forward or backwards.
  var direction = Blockly.Language.maze_move.DIRECTIONS
      .indexOf(this.getTitleText(1));
  return 'Maze.move(' + direction + ', "' + this.id + '");\n';
};

Blockly.Language.maze_turnLeft = {
  // Block for turning left or right.
  category: 'Maze',
  helpUrl: 'http://code.google.com/p/google-blockly/wiki/Turn',
  init: function() {
    this.setColour(290);
    this.addTitle('turn');
    var dropdown = new Blockly.FieldDropdown(function() {
      return Blockly.Language.maze_turnLeft.DIRECTIONS;
    });
    this.addTitle(dropdown);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Turns the mouse left or right by 90 degrees.');
  }
};

Blockly.Language.maze_turnLeft.DIRECTIONS = ['left', 'right'];

Blockly.Language.maze_turnRight = {
  // Block for turning left or right.
  category: 'Maze',
  helpUrl: null,
  init: function() {
    this.setColour(290);
    this.addTitle('turn');
    var dropdown = new Blockly.FieldDropdown(function() {
      return Blockly.Language.maze_turnLeft.DIRECTIONS;
    });
    this.addTitle(dropdown);
    this.setTitleText(Blockly.Language.maze_turnLeft.DIRECTIONS[1], 1);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Turns the mouse left or right by 90 degrees.');
  }
};

Blockly.JavaScript.maze_turnLeft = function() {
  // Generate JavaScript for turning left or right.
  var direction = Blockly.Language.maze_turnLeft.DIRECTIONS
      .indexOf(this.getTitleText(1));
  return 'Maze.turn(' + direction + ', "' + this.id + '");\n';
};

// Turning left and right use the same code.
Blockly.JavaScript.maze_turnRight = Blockly.JavaScript.maze_turnLeft;

Blockly.Language.maze_isWall = {
  // Block for checking if there a wall.
  category: 'Maze',
  helpUrl: 'http://code.google.com/p/google-blockly/wiki/Wall',
  init: function() {
    this.setColour(290);
    this.setOutput(true);
    this.addTitle('wally');
    var dropdown = new Blockly.FieldDropdown(function() {
      return Blockly.Language.maze_isWall.DIRECTIONS;
    });
    this.addTitle(dropdown);
    this.setTooltip('Returns true if there is a wall in ' +
                    'the specified direction.');
  }
};

Blockly.Language.maze_isWall.DIRECTIONS =
    ['ahead', 'to the left', 'to the right', 'behind'];

Blockly.JavaScript.maze_isWall = function() {
  // Generate JavaScript for checking if there is a wall.
  var direction = Blockly.Language.maze_isWall.DIRECTIONS
      .indexOf(this.getTitleText(1));
  return 'Maze.isWall(' + direction + ')';
};

/////////////////////////////////////////////////////

Blockly.Language.maze_foo = {
  // Block for checking if there a wall.
  category: 'xk',
  helpUrl: 'http://code.google.com/p/google-blockly/wiki/Wall',
  init: function() {
    this.setColour(294);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.addTitle('foo');
    var dropdown = new Blockly.FieldDropdown(function() {
      return Blockly.Language.maze_foo.DIRECTIONS;
    });
    this.addTitle(dropdown);
    this.setTooltip('Returns true if there is a wall in ' +
                    'the specified direction.');
  }
};

Blockly.Language.maze_foo.DIRECTIONS =
    ['foodle', 'doodle', 'woo'];

Blockly.JavaScript.maze_foo = function() {
  // Generate JavaScript for checking if there is a wall.
  var foo = Blockly.Language.maze_foo.DIRECTIONS
      .indexOf(this.getTitleText(1));
  return 'alert(' + foo + ')';
};

*/
